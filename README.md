spamdetector
================


Here are two attack vectors:

- Spammers send the same message to multiple users within one minute. 

- Spammers rate limit their spam message to not exceed the threshold of 50 per minute and randomize their messages to be undetected

Please come up with a strategy to address those two vectors.

Write the interface, implementation and test classes.

Here's the format of a message in Json:

{"sourceProfileId":"35603735","targetProfileId":"36872220","sourceClientId":"undefined","messageId":"5EFFB930-4B28-4B80-861B-760787188D29","type":"text","timestamp":1406047430609,"body":"Hello Munir! Checkout my site http://helloworld.com"}

