package com.grindr.spamdetector;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.grindr.spamdetector.rest.domain.Message;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Spam detector test cases
 * 
 * @author Amir Tajer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class SpamDetectorTest {
	
	private static Random random = new Random();
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	
	private MockMvc mockMvc;
	
//	private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
    	this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void testSameMessageBodyAttack() throws Exception {
    	
    	for(int i=0; i<2; i++) {
    		
    		String from = "35603735";
    		String to = "3687" + generateRandomCode();
    		String body = "Hello!";
    		
        	Message message = generateMessage(from, to, body);
        	
    		String messageJson = message.getJson().toString();
            
            this.mockMvc.perform(post("/messages/check")
                    .contentType(contentType)
                    .content(messageJson))
                    .andExpect(status().isOk());
            
            Thread.sleep(5000);
    	}
    	
    }
    
    @Test
    public void testRandomMessageBodyAttack() throws Exception {
    	
    	for(int i=0; i<55; i++) {
    		
    		String from = "35603735";
    		String to = "3687" + generateRandomCode();
    		String body = "Hello " + to;
    		
        	Message message = generateMessage(from, to, body);
        	
    		String messageJson = message.getJson().toString();
            
            this.mockMvc.perform(post("/messages/check")
                    .contentType(contentType)
                    .content(messageJson))
                    .andExpect(status().isOk());
            
            Thread.sleep(1000);
//            Thread.sleep(1500);
    	}
    	
    }
    
    private Message generateMessage(String from, String to, String body) {
    	
    	Message message = new Message();
    	message.setSourceProfileId(from);
    	message.setTargetProfileId(to);
    	message.setSourceClientId("undefined");
    	message.setMessageId(UUID.randomUUID().toString());
    	message.setType("text");
    	message.setTimestamp(new Date().getTime());
    	message.setBody(body);
    	
    	return message;
    }
    
    private String generateRandomCode() {
		//Random random = new Random();
		int code = random.nextInt(8999) + 1000;
		return String.valueOf(code);
	}
    

}
