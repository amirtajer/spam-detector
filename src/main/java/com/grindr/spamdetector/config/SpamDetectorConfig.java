package com.grindr.spamdetector.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpamDetectorConfig {

    @Value("${spamdetector.cycle}")
    private String checkingCycle;

    @Value("${spamdetector.threshold}")
    private String randomAttackThreshold;

    
    public long getCheckingCycle() {
        return Long.valueOf(checkingCycle);
    }

    public int getRandomAttackThreshold() {
        return Integer.valueOf(randomAttackThreshold);
    }

}
