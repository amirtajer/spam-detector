package com.grindr.spamdetector.rest.domain;

import org.json.JSONObject;
import org.springframework.util.StringUtils;

public class Message {

	private String sourceProfileId;
	private String targetProfileId;
	private String sourceClientId;
	private String messageId;
	private String type;
	private long timestamp;
	private String body;
	
	public void validate() throws IllegalArgumentException {
		if(StringUtils.isEmpty(sourceProfileId))
			throw new IllegalArgumentException("sourceProfileId is empty.");
		if(StringUtils.isEmpty(targetProfileId))
			throw new IllegalArgumentException("targetProfileId is empty.");
		if(StringUtils.isEmpty(messageId))
			throw new IllegalArgumentException("messageId is empty.");

	}
	
//	public String toString() {
//	
//	}

	public JSONObject getJson() {
		JSONObject json = new JSONObject();
		json.put("sourceProfileId", sourceProfileId);
		json.put("targetProfileId", targetProfileId);
		json.put("sourceClientId", sourceClientId);
		json.put("messageId", messageId);
		json.put("type", type);
		json.put("timestamp", timestamp);
		json.put("body", body);
		return json;
	}


	public String getSourceProfileId() {
		return sourceProfileId;
	}

	public void setSourceProfileId(String sourceProfileId) {
		this.sourceProfileId = sourceProfileId;
	}

	public String getTargetProfileId() {
		return targetProfileId;
	}

	public void setTargetProfileId(String targetProfileId) {
		this.targetProfileId = targetProfileId;
	}

	public String getSourceClientId() {
		return sourceClientId;
	}

	public void setSourceClientId(String sourceClientId) {
		this.sourceClientId = sourceClientId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}
