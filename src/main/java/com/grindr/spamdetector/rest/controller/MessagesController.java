package com.grindr.spamdetector.rest.controller;


import com.grindr.spamdetector.service.SpamDetectorService;
import com.grindr.spamdetector.rest.domain.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Messages controller
 *
 * @author Amir Tajer
 * 
 */
@RestController
@RequestMapping("/messages")
public class MessagesController {

    private static final Logger LOGGER =  LoggerFactory.getLogger(MessagesController.class);
    
    private final SpamDetectorService spamDetectorService;

    @Autowired
    public MessagesController(SpamDetectorService spamDetectorService) {
        this.spamDetectorService = spamDetectorService;
    }
    
    @RequestMapping( value="/check", method = RequestMethod.POST )
	public ResponseEntity<String> checkMessage(@RequestBody Message messageData) {
    		
    	try {
    		LOGGER.info("Checking message...");
    		
    		messageData.validate();
    		
    		String result = spamDetectorService.process(messageData);
    		
            return new ResponseEntity<String>(result, HttpStatus.OK);
    		
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    	} catch(Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
    
}
