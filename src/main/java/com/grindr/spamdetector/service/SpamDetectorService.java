package com.grindr.spamdetector.service;

import com.grindr.spamdetector.rest.domain.Message;

public interface SpamDetectorService {
	
	String process(Message message);
	
}