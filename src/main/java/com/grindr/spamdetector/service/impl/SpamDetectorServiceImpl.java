package com.grindr.spamdetector.service.impl;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import com.grindr.spamdetector.config.SpamDetectorConfig;
import com.grindr.spamdetector.rest.domain.Message;
import com.grindr.spamdetector.model.MessageLog;
import com.grindr.spamdetector.service.SpamDetectorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Spam detector service implementation
 *
 * @author Amir Tajer
 */
@Service
public class SpamDetectorServiceImpl implements SpamDetectorService {

    private static final Logger LOGGER =  LoggerFactory.getLogger(SpamDetectorService.class);
    
    private final SpamDetectorConfig mConfig;
    
    private final long mCheckingCycle;
    private final int mRandomAttackTreshold;
    
    private Map<String, MessageLog> mMessageMap;
    
    @Autowired
    public SpamDetectorServiceImpl(final SpamDetectorConfig config) {
        this.mConfig = config;
        this.mCheckingCycle = config.getCheckingCycle();
    	this.mRandomAttackTreshold = config.getRandomAttackThreshold();
        this.mMessageMap = new HashMap<String, MessageLog>();
    }

    @PreDestroy
    public void preDestroy() {
        if (this.mMessageMap != null) {
        	this.mMessageMap.clear();
        	this.mMessageMap = null;
        }
    }
    
    
	@Override
    public String process(Message message) {
    	
    	//message.validate();
    	if(message == null) return null;
    	
    	String sourceProfileId = message.getSourceProfileId();
    	String messageBody = message.getBody();
    	
    	LOGGER.info("processing message. sourceProfileId: " + sourceProfileId + " messageBody: " + messageBody);
    	
    	if(!mMessageMap.containsKey(sourceProfileId)) {
    		
    		//put new messageLog for this message
    		MessageLog messageLog = generateMessageLog(message, 1);
    		mMessageMap.put(sourceProfileId, messageLog);
    		
    	}else{
    		
    		MessageLog messageLog = mMessageMap.get(sourceProfileId);
    		
    		if(messageLog != null) {
    			
    			//check if current message time is within one minute from last message for the same sourceProfileId 
    			if(messageLog.getLastSentTime() >= message.getTimestamp() - mCheckingCycle) {
    				
    				if(messageBody.equals(messageLog.getBody())) {
    					
    		    		//message body is the same within one minute so it is spam
    					reportSpammer(sourceProfileId);
    		    		
        	    		messageLog.setLastSentTime(message.getTimestamp());
        	    		messageLog.setLastSentCount(1);
        	    		
    				}else{
    					
		    			int count = messageLog.getLastSentCount();
		    			
    		    		if(count < mRandomAttackTreshold ) {
    		    			
    	    	    		messageLog.setLastSentCount(count + 1);
    		    			
    		    		}else{
        		    		//maximum number of message within one minute reached so it is spam
        					reportSpammer(sourceProfileId);
        					
            	    		messageLog.setLastSentTime(message.getTimestamp());
            	    		messageLog.setLastSentCount(1);
    		    		}
    		    		
    				}
    				
    	    	}else{
    	    		messageLog.setBody(message.getBody());
    	    		messageLog.setLastSentTime(message.getTimestamp());
    	    		messageLog.setLastSentCount(1);
    	    	}
    			
    	    	LOGGER.info("messageLog-> message count within cycle: " + messageLog.getLastSentCount());
    			
    			//update MessageLog
    			mMessageMap.remove(sourceProfileId);
    			mMessageMap.put(sourceProfileId, messageLog);
    			
    		}
    		
    	}
    	
    	return message.getMessageId();
    }
    
    
    private MessageLog generateMessageLog(Message message, int count) {
		MessageLog messageLog = new MessageLog();
		messageLog.setSourceProfileId(message.getSourceProfileId());
		messageLog.setBody(message.getBody());
		messageLog.setLastSentTime(message.getTimestamp());
		messageLog.setLastSentCount(count);
		
    	return messageLog;
    }
    
    private void reportSpammer(String profileId) {
    	
    	//report this profileId as spammer
    	LOGGER.info(">>>>>>>> Spammer detected. -> profileId: " + profileId);
    }
    
    
    
}
