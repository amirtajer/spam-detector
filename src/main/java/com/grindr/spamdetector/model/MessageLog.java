package com.grindr.spamdetector.model;


public class MessageLog {
	
	private String sourceProfileId;
	private String body;
	private long lastSentTime;
	private int lastSentCount;
	
//	private Message message;
	
	
	
//	public String toString() {
//	
//	}



	public String getSourceProfileId() {
		return sourceProfileId;
	}

	public void setSourceProfileId(String sourceProfileId) {
		this.sourceProfileId = sourceProfileId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public long getLastSentTime() {
		return lastSentTime;
	}

	public void setLastSentTime(long lastSentTime) {
		this.lastSentTime = lastSentTime;
	}

	public int getLastSentCount() {
		return lastSentCount;
	}

	public void setLastSentCount(int lastSentCount) {
		this.lastSentCount = lastSentCount;
	}
	
}
