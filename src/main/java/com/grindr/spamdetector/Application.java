package com.grindr.spamdetector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main Application class for spamdetector
 *
 * @author Amir Tajer
 * @since 1.0
 */
@EnableAutoConfiguration
@ComponentScan
public class Application {
    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }
}
